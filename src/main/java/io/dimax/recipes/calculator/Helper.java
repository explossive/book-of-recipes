package io.dimax.recipes.calculator;

import java.util.ArrayList;
import java.util.List;

public class Helper {

    public static List<Lifestyle> getLifestyles(){
        List<Lifestyle> lifestyle = new ArrayList<>();
        lifestyle.add(new Lifestyle(0, "Не знаю", 1.0));
        lifestyle.add(new Lifestyle(1, "Сидячий, малоподвижный", 1.2));
        lifestyle.add(new Lifestyle(2, "Легкая активность (упражнения 1-3 раза в неделю)", 1.4));
        lifestyle.add(new Lifestyle(3, "Средняя активность (тренировки 3-5 раз в неделю)", 1.6));
        lifestyle.add(new Lifestyle(4, "Высокая активность (высокие нагрузки каждый день)", 1.7));
        lifestyle.add(new Lifestyle(5, "Экстремально высокая активность", 1.8));
        return lifestyle;
    }

    public static List<Purpose> getPurposes(){
        List<Purpose> purposes = new ArrayList<>();
        purposes.add(new Purpose(0, "Не знаю"));
        purposes.add(new Purpose(1, "Сбросить вес"));
        purposes.add(new Purpose(2, "Набрать мышечную массу"));
        purposes.add(new Purpose(3, "Поддерживать вес"));
        return purposes;
    }

    public static Lifestyle getLifestyleById(int id){
        for(Lifestyle lifestyle : getLifestyles()){
            if(lifestyle.getId() == id){
                return lifestyle;
            }
        }
        return null;
    }
}
