package io.dimax.recipes.calculator;

public class Lifestyle {

    private int id;

    private String title;

    private Double a;

    public Lifestyle(int id, String title, Double a) {
        this.id = id;
        this.title = title;
        this.a = a;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getA() {
        return a;
    }

    public void setA(Double a) {
        this.a = a;
    }
}
