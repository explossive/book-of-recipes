package io.dimax.recipes.calculator;

public class CalculatorCaloricService implements CalculatorService {

    private Gender gender;

    private Integer weight;

    private Integer growth;

    private Integer age;

    private Lifestyle lifestyle;

    public CalculatorCaloricService(Gender gender, Integer weight, Integer growth, Integer age, Lifestyle lifestyle) {
        this.gender = gender;
        this.weight = weight;
        this.growth = growth;
        this.age = age;
        this.lifestyle = lifestyle;
    }

    public Integer getCalories()
    {
        Double result = (10 * weight + 6.25 * growth - 5 * age + getCoefficient()) * lifestyle.getA();

        return result.intValue();
    }

    public Integer getBjo()
    {
        Double result = (10 * weight + 6.25 * growth - 5 * age + getCoefficient()) * lifestyle.getA();

        return result.intValue();
    }

    private Integer getCoefficient(){
        if(Gender.MALE == gender){
            return 5;
        }
        return -161;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getGrowth() {
        return growth;
    }

    public void setGrowth(Integer growth) {
        this.growth = growth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Lifestyle getLifestyle() {
        return lifestyle;
    }

    public void setLifestyle(Lifestyle lifestyle) {
        this.lifestyle = lifestyle;
    }
}
