package io.dimax.recipes.calculator;

public enum Gender {
    MALE,
    FEMALE
}
