package io.dimax.recipes.calculator;

public interface CalculatorService {

    public Integer getCalories();

}
