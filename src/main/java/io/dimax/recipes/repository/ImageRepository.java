package io.dimax.recipes.repository;

import io.dimax.recipes.model.Image;

import java.util.List;

public interface ImageRepository {

    Image findById(int id);

    List<Image> findAll();

    int deleteById(int id);

    void create(Image ingredient);

    void update(Image ingredient);

    List<Image> findByTitle(String title);
}

