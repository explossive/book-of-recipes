package io.dimax.recipes.repository;

import io.dimax.recipes.model.Ingredient;

import java.util.List;

public interface IngredientRepository {

    Ingredient findById(int id);

    List<Ingredient> findAll();

    int deleteById(int id);

    void create(Ingredient ingredient);

    void update(Ingredient ingredient);

    List<Ingredient> findByTitle(String title);
}

