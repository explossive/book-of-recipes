package io.dimax.recipes.repository;

import io.dimax.recipes.model.RecipeIngredient;

import java.util.List;

public interface RecipeIngredientRepository {

    RecipeIngredient findById(int id);

    List<RecipeIngredient> findAll();

    int deleteById(int id);

    void create(RecipeIngredient ingredient);

    void update(RecipeIngredient ingredient);

    List<RecipeIngredient> findByTitle(String title);
}

