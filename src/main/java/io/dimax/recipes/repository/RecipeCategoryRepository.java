package io.dimax.recipes.repository;

import io.dimax.recipes.model.RecipeCategory;

import java.util.List;

public interface RecipeCategoryRepository {

    RecipeCategory findById(int id);

    List<RecipeCategory> findAll();

    int deleteById(int id);

    void create(RecipeCategory recipe);

    void update(RecipeCategory recipe);

    List<RecipeCategory> findByTitle(String title);
}

