package io.dimax.recipes.repository;

import io.dimax.recipes.model.Unit;

import java.util.List;

public interface UnitRepository {

    Unit findById(int id);

    List<Unit> findAll();

    int deleteById(int id);

    void create(Unit ingredient);

    void update(Unit ingredient);

    List<Unit> findByTitle(String title);
}

