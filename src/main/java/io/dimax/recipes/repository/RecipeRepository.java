package io.dimax.recipes.repository;

import io.dimax.recipes.model.Recipe;

import java.util.List;

public interface RecipeRepository {

    Recipe findById(int id);

    List<Recipe> findAll();

    int deleteById(int id);

    void create(Recipe recipe);

    void update(Recipe recipe);

    List<Recipe> findByTitle(String title);
}

