package io.dimax.recipes.repository;

import io.dimax.recipes.model.RecipeStage;

import java.util.List;

public interface RecipeStageRepository {

    RecipeStage findById(int id);

    List<RecipeStage> findAll();

    int deleteById(int id);

    void create(RecipeStage ingredient);

    void update(RecipeStage ingredient);

    List<RecipeStage> findByTitle(String title);
}

