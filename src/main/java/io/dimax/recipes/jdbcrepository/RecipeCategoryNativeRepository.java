package io.dimax.recipes.jdbcrepository;

import io.dimax.recipes.model.RecipeCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository("recipeCategoryRepository")
public class RecipeCategoryNativeRepository implements io.dimax.recipes.repository.RecipeCategoryRepository {

    @Autowired
    private EntityManager em;

    @Override
    public RecipeCategory findById(int id) {
        RecipeCategory recipeCategory = (RecipeCategory) em.createNativeQuery("SELECT * FROM recipe_category as g where g.id =?", RecipeCategory.class)
                .setParameter(1, id)
                .getSingleResult();
        return recipeCategory;
    }

    @Override
    public List<RecipeCategory> findAll() {
        List<RecipeCategory> recipeCategories = em.createNativeQuery("SELECT * FROM recipe_category", RecipeCategory.class)
                .getResultList();
        return recipeCategories;
    }

    @Transactional
    @Override
    public void create(RecipeCategory recipeCategory) {
        em.persist(recipeCategory);
    }

    @Transactional
    @Override
    public void update(RecipeCategory recipeCategory) {
        em.persist(recipeCategory);
    }

    @Transactional
    @Override
    public int deleteById(int id) {
        Query query = em.createNativeQuery("Delete from recipe_category where id=?");
        query.setParameter(1, id);
        return query.executeUpdate();
    }

    @Override
    public List<RecipeCategory> findByTitle(String title) {
        List<RecipeCategory> recipeCategories = em.createNativeQuery("SELECT * FROM recipe_category as g where g.title LIKE ? ", RecipeCategory.class)
                .setParameter(1, "'%"+title+"%'")
                .getResultList();
        return recipeCategories;
    }
}


