package io.dimax.recipes.jdbcrepository;

import io.dimax.recipes.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository("recipeRepository")
public class RecipeNativeRepository implements io.dimax.recipes.repository.RecipeRepository {

    @Autowired
    private EntityManager em;

    @Override
    public Recipe findById(int id) {
        Recipe recipe = (Recipe) em.createNativeQuery("SELECT * FROM recipe as g where g.id =?", Recipe.class)
                .setParameter(1, id)
                .getSingleResult();
        return recipe;
    }

    @Override
    public List<Recipe> findAll() {
        List<Recipe> recipes = em.createNativeQuery("SELECT * FROM recipe", Recipe.class)
                .getResultList();
        return recipes;
    }

    @Transactional
    @Override
    public void create(Recipe recipe) {
        em.persist(recipe);
    }

    @Transactional
    @Override
    public void update(Recipe recipe) {
        em.persist(recipe);
    }

    @Transactional
    @Override
    public int deleteById(int id) {
        Query query = em.createNativeQuery("Delete from recipe where id=?");
        query.setParameter(1, id);
        return query.executeUpdate();
    }

    @Override
    public List<Recipe> findByTitle(String title) {
        List<Recipe> recipes = em.createNativeQuery("SELECT * FROM recipe as g where g.title LIKE ? ", Recipe.class)
                .setParameter(1, "'%"+title+"%'")
                .getResultList();
        return recipes;
    }
}


