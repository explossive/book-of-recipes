package io.dimax.recipes.jdbcrepository;

import io.dimax.recipes.model.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository("ingredientRepository")
public class IngredientNativeRepository implements io.dimax.recipes.repository.IngredientRepository {

    @Autowired
    private EntityManager em;

    @Override
    public Ingredient findById(int id) {
        Ingredient ingredient = (Ingredient) em.createNativeQuery("SELECT * FROM ingredient as g where g.id =?", Ingredient.class)
                .setParameter(1, id)
                .getSingleResult();
        return ingredient;
    }

    @Override
    public List<Ingredient> findAll() {
        List<Ingredient> ingredients = em.createNativeQuery("SELECT * FROM ingredient", Ingredient.class)
                .getResultList();
        return ingredients;
    }

    @Transactional
    @Override
    public void create(Ingredient ingredient) {
        em.persist(ingredient);
    }

    @Transactional
    @Override
    public void update(Ingredient ingredient) {
        em.persist(ingredient);
    }

    @Transactional
    @Override
    public int deleteById(int id) {
        Query query = em.createNativeQuery("Delete from ingredient where id=?");
        query.setParameter(1, id);
        return query.executeUpdate();
    }

    @Override
    public List<Ingredient> findByTitle(String title) {
        List<Ingredient> ingredients = em.createNativeQuery("SELECT * FROM ingredient as g where g.title LIKE ? ", Ingredient.class)
                .setParameter(1, "'%"+title+"%'")
                .getResultList();
        return ingredients;
    }
}


