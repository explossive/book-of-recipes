package io.dimax.recipes.form;

import io.dimax.recipes.calculator.Gender;
import io.dimax.recipes.calculator.Helper;
import io.dimax.recipes.calculator.Lifestyle;

import javax.validation.constraints.NotNull;

public class CalculatorForm {

    @NotNull
    private String gender;

    @NotNull
    private Integer age;

    @NotNull
    private Integer growth;

    @NotNull
    private Integer weight;

    @NotNull
    private Integer lifestyleId;

    @NotNull
    private Integer purposeId;


    public Gender getGenderValue() {
        if(gender.equals("male")){
            return Gender.MALE;
        }
        return Gender.FEMALE;
    }

    public Lifestyle getLifestyle() {
        return Helper.getLifestyleById(lifestyleId);
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGrowth() {
        return growth;
    }

    public void setGrowth(Integer growth) {
        this.growth = growth;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }


    public Integer getLifestyleId() {
        return lifestyleId;
    }

    public void setLifestyleId(Integer lifestyleId) {
        this.lifestyleId = lifestyleId;
    }

    public Integer getPurposeId() {
        return purposeId;
    }

    public void setPurposeId(Integer purposeId) {
        this.purposeId = purposeId;
    }
}
