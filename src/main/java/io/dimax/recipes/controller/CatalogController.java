package io.dimax.recipes.controller;

import io.dimax.recipes.calculator.Helper;
import io.dimax.recipes.form.CalculatorForm;
import io.dimax.recipes.model.User;
import io.dimax.recipes.repository.RecipeCategoryRepository;
import io.dimax.recipes.repository.RecipeRepository;
import io.dimax.recipes.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

@Controller
public class CatalogController {

    @Autowired
    private UserService userService;

    @Autowired
    private RecipeCategoryRepository categoryRepository;

    @Autowired
    private RecipeRepository recipeRepository;

    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("user", userService.getAuthUser());
        model.addAttribute("recipes", recipeRepository.findAll());
        return "catalog/index";
    }

    @RequestMapping(value = "/catalog/{id}", method = RequestMethod.GET)
    public String index(Model model, @PathVariable("id") int id) {
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("user", userService.getAuthUser());
        return "catalog/category";
    }

    @RequestMapping(value = "/category/all", method = RequestMethod.GET)
    public String categories(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("user", userService.getAuthUser());
        model.addAttribute("recipes", recipeRepository.findAll());
        return "catalog/category";
    }

    @RequestMapping(value = "/recipe/{id}", method = RequestMethod.GET)
    public String recipeView(Model model, @PathVariable("id") int id) {
        model.addAttribute("recipes", recipeRepository.findAll());
        model.addAttribute("user", userService.getAuthUser());
        return "catalog/index";
    }
}
