package io.dimax.recipes.controller;

import io.dimax.recipes.calculator.CalculatorCaloricService;
import io.dimax.recipes.calculator.Helper;
import io.dimax.recipes.form.CalculatorForm;
import io.dimax.recipes.model.User;
import io.dimax.recipes.repository.RecipeCategoryRepository;
import io.dimax.recipes.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import javax.validation.Valid;

@Controller
public class MainController {

    @Autowired
    private RecipeCategoryRepository recipeCategoryRepository;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexDefinitions(Model model) {
        CalculatorForm calculatorForm = new CalculatorForm();
        model.addAttribute("calculatorForm", calculatorForm);
        model.addAttribute("recipeCategories", recipeCategoryRepository.findAll());
        model.addAttribute("lifestyles", Helper.getLifestyles());
        model.addAttribute("purposes", Helper.getPurposes());
        model.addAttribute("user", userService.getAuthUser());
        return "index";
    }

    @PostMapping(value = "/")
    public String calculateCalories(@ModelAttribute @Valid CalculatorForm form, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("recipeCategories", recipeCategoryRepository.findAll());
            model.addAttribute("lifestyles", Helper.getLifestyles());
            model.addAttribute("purposes", Helper.getPurposes());
            model.addAttribute("user", userService.getAuthUser());
            return "index";
        } else {
            CalculatorCaloricService c = new CalculatorCaloricService(
                    form.getGenderValue(),
                    form.getWeight(),
                    form.getGrowth(),
                    form.getAge(),
                    form.getLifestyle()
            );
            model.addAttribute("data", c.getCalories());
            return "ok";
        }
    }
}
