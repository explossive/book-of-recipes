package io.dimax.recipes.controller;

import io.dimax.recipes.calculator.Helper;
import io.dimax.recipes.form.CalculatorForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PersonalController {

    @RequestMapping(value = "/personal", method = RequestMethod.GET)
    public String index(Model model) {
        return "personal/index";
    }

    @RequestMapping(value = "/personal/calendar", method = RequestMethod.GET)
    public String calendar(Model model) {
        return "personal/calendar";
    }
}
