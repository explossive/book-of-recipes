$().ready(function() {
    var options = {
        height: 1000,
        width: 1200,
        navHeight: 25,
        labelHeight: 25,
        onMonthChanging: function(dateIn) {
            //this could be an Ajax call to the backend to get this months events
            //var events = [ 	{ "EventID": 7, "StartDate": new Date(2009, 1, 1), "Title": "10:00 pm - EventTitle1", "URL": "#", "Description": "This is a sample event description", "CssClass": "Birthday" },
            //				{ "EventID": 8, "StartDate": new Date(2009, 1, 2), "Title": "9:30 pm - this is a much longer title", "URL": "#", "Description": "This is a sample event description", "CssClass": "Meeting" }
            //];
            //$.jMonthCalendar.ReplaceEventCollection(events);
            return true;
        },
        onEventBlockOver: function(event) {
            //alert(event.Title + " - " + event.Description);
            return true;
        },
        onEventBlockOut: function(event) {
            return true;
        },
        onDayLinkClick: function(date) {
            return true;
        },
        onDayCellClick: function(date) {
            return true;
        }
    };


    var events = [ 	{ "EventID": 1, "Date": "new Date(2009, 1, 1)", "Title": "10:00 pm - EventTitle1", "URL": "#", "Description": "This is a sample event description", "CssClass": "Birthday" },
        { "EventID": 1, "StartDate": new Date(2009, 1, 12), "Title": "10:00 pm - EventTitle1", "URL": "#", "Description": "This is a sample event description", "CssClass": "Birthday" },
        { "EventID": 2, "Date": "2009-02-28T00:00:00.0000000", "Title": "9:30 pm - this is a much longer title", "URL": "#", "Description": "This is a sample event description", "CssClass": "Meeting" },
        { "EventID": 3, "StartDate": new Date(2009, 1, 20), "Title": "9:30 pm - this is a much longer title", "URL": "#", "Description": "This is a sample event description", "CssClass": "Meeting" }
    ];

    var newoptions = { };
    var newevents = [ ];
    //$.jMonthCalendar.Initialize(newoptions, newevents);


    $.jMonthCalendar.Initialize(options, events);




    var extraEvents = [	{ "EventID": 5, "StartDate": new Date(2019, 4, 11), "Title": "10:00 pm - EventTitle1", "URL": "#", "Description": "This is a sample event description", "CssClass": "Birthday" },
        { "EventID": 6, "StartDate": new Date(2019, 4, 20), "Title": "9:30 pm - this is a much longer title", "URL": "#", "Description": "This is a sample event description", "CssClass": "Meeting" }
    ];

    $("#Button").click(function() {
        $.jMonthCalendar.AddEvents(extraEvents);
    });

    $("#ChangeMonth").click(function() {
        $.jMonthCalendar.ChangeMonth(new Date(2019, 4, 4));
    });
});